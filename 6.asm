extern scanf
extern printf

section .data
	req: db "Enter number",10,0	
	formatstr: db "%s",0
	formatdec: db "%d",0
	out: db "The %dth armstrong number is: %d",10,0
	inp: dd 0
	counter: dd 0
	sum: dd 0
	num: dd 0
	rem: dd 0
	a: dd 0
	b: dd 1
	iterator: dd 1
	
section .text 

	global main
	main:

	push ebp
	mov ebp, esp
	
	push req
	push formatstr
	call printf

	push inp
	push formatdec
	call scanf
					

	mov edx, 0x0

	l1:	
		mov ecx, DWORD[iterator]
		mov DWORD[num],ecx
		mov DWORD[sum],0x0	
		l2:
			mov eax, DWORD[num]
			mov ebx, 0xa
			xor edx, edx
			cdq
			idiv ebx
			mov DWORD[num], eax
			mov ebx,0x1
			mov DWORD[rem],edx
			imul ebx,DWORD[rem]
			imul ebx,DWORD[rem]
			imul ebx,DWORD[rem]
			mov edx, DWORD[sum]
			add edx,ebx
			mov DWORD[sum], edx
			cmp DWORD[num],0x0
				je l3
			jmp l2
		l3:
		mov ecx, DWORD[iterator]
		cmp DWORD[sum],ecx
			jne l5
			l4:
				mov edx, DWORD[counter]
				inc edx
				mov DWORD[counter], edx
		l5:
		mov edx, DWORD[counter]
		cmp edx, DWORD[inp]	
			je l6
		mov ecx, DWORD[iterator]		
		inc ecx 		
		mov DWORD[iterator], ecx
		jmp l1		
	l6:

	push DWORD[iterator]
	push DWORD[inp]
	push out
	call printf

	mov esp, ebp
	pop ebp
	ret
