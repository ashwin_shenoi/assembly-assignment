extern scanf
extern printf

section .data
	req: db "Enter number",10,0	
	formatstr: db "%s",0
	formatdec: db "%d",0
	out: db "The sum of numbers from 1 to %d is: %d",10,0
	inp: dd 0
	
section .text 

	global main
	main:

	push ebp
	mov ebp, esp
	
	push req
	push formatstr
	call printf

	push inp
	push formatdec
	call scanf

	mov ebx, DWORD[inp]
	
	mov eax,0x0

	mov ecx, 0x1
	l1:	
	cmp ecx,ebx
	je l2	
	add eax,ecx
	add ecx,0x1
	jmp l1
	l2:

	add eax,ecx

	push eax
	push ebx
	push out
	call printf

	mov esp, ebp
	pop ebp
	ret
